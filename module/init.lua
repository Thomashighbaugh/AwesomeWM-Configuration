return {
    auto_start = require('module.auto-start'),
    decorate_client = require('module.decorate-client'),
    exit_screen = require('module.exit-screen'),
    lockscreen = require('module.lockscreen'),
    menu = require('module.menu'),
    notifications = require('module.notifications'),
    quake_terminal = require('module.quake-terminal'),
    titlebar = require('module.titlebar'),

}
