# Awesome Window Manager Configuration


> An awesome configuration sans bullocks. 
> 

Part of [my dotfiles]( https://github.com/Thomashighbaugh/dotfiles), provisioned by Makefile thus easily split out into another repo ;]


>  _______                                             ________ _______ 
> |   _   |.--.--.--.-----.-----.-----.--------.-----.|  |  |  |   |   |
> |       ||  |  |  |  -__|__ --|  _  |        |  -__||  |  |  |       |
> |___|___||________|_____|_____|_____|__|__|__|_____||________|__|_|__|
                                                                      


## What This Is

> Flying Spaghetti Monster of Code
 

A modular configuration for Awesome Window Manager that is stylized according to my taste, functional according to my workflow, stripped of anything I found frivilous with low overhead and lots of ASCii art to boot.

## Key Bindings

| Key | Function |
|-----|----------|
| mod + Insert | Display Key Bindings Help |
| mod + r | Awesome Restart |
| mod + Control + q | Awesome Quit |
| mod + Up| Increase Width Factor of Group |
| mod + Down| Decrease Width Factor of Group |
| mod + Shift + l | Increase Number of master clients |
| mod + Shift + h | Decrease Number of master clients |
| mod + Control + h | Increase Number of Columns |
| mod + Control + l | Decrease Number of Columns |
| mod + Space | Next Layout |
| mod + Shift + Space | Previous Layout |
| mod + Shift + 1 | Focus on next screen |
| mod + Shift + 2 | Focus on prior screen |
| mod + ` | Dropdown Terminal | 
| Print | Fullscreen Screenshot |
| mod + Print | Selection Screenshot |
| mod + Shift + ] | Toggle Blur |
| mod + ] | Increase Blur |
| mod + [ | Decrease Blur |
| mod | Rofi Application Sidebar |
| mod + Return | Kitty |
| mod + F2 | Firefox |
| mod + Shift + F2 | Chromium |
| mod + F3 | Thunar |
| mod + Shift + F3 | Root Ranger |
| mod + F4 | Font Awesome Icon Selector |
| mod + Shift + F4 | Emoji Picker |
| mod + F5 | Picom On |
| mod + Shift + F5 | Kill Picom |
| mod + F6 | Network Menu Rofi | 
| mod + Alt + Delete | Spawn htop |
| mod + Home | Global Search | 
| mod 1-9 | Select and view tag |
| mod + Shift 1-9 | Select and view tag |

| Keys | Function |
|------|----------|
| mod + f | toggle fullscreen |
| mod + x | kill window |
| mod + d | next client focus |
| mod + a | previous client index |
| mod + Shift + d | swap with next client by index |
| mod + Shift + a | swap with previous client by index |
| mod + u | Go to urgent client |
| mod + Tab | go to previous focused on target |
| mod + n | minimize client |
| mod + Control + n | restore client |
| mod + Shift + c | align a client to the center of the focused screen |
| mod + c | toggle floating client |
| mod + Alt + Up | move floating client up by 10 px |
| mod + Alt + Down | move floating client down by 10 px |
| mod + Alt + Right | move floating client right by 10 px |
| mod + Alt + Left | move floating client left by 10 px |
| mod + Shift + Up | increase floating client size vertically by 10 px up |
| mod + Shift + Down | increase floating client size vertically by 10 px down |
| mod + Shift + Right | increase floating client size horizontally by 10 px right |
| mod + Shift + Left | increase floating client size horizontally by 10 px left |
| mod + Control + Up | decrease floating client size vertically by 10 px up |
| mod + Control + Down | decrease floating client size vertically by 10 px down |
| mod + Control + Right | decrease floating client size horizontally by 10 px right |
| mod + Control + Left | decrease floating client size horizontally by 10 px left |


